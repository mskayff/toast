// ----------------------------------------------------------------------------------------
// Globally define Toast
// ----------------------------------------------------------------------------------------
var Toast;

(function( $ ) {

	Toast = {

	_toast : null,

	_timer : null,

	_icons : {
			warning: 'warning.webp'
		},

	show : function(msg, options){

		var self = this;
		var messageContent = "";

		// ----------------------------------------------------------------------------------------
		// Clear any previous timer
		// ----------------------------------------------------------------------------------------
		clearTimeout(this._timer);

		// ----------------------------------------------------------------------------------------
		// Remove any pre-existing Toast
		// ----------------------------------------------------------------------------------------
		if(this._toast != null)
			this.removeToast();

		// ----------------------------------------------------------------------------------------
		// Merge the options passed as the second param with a default set of options
		// ----------------------------------------------------------------------------------------
		var options = $.extend({
				style :   'dark',							
				overlay_style : 'none',// Options are none, transparent, opaque
				position : 'center',
				duration : 1000,
				no_icon: false,
			}, options);

		// ----------------------------------------------------------------------------
		// Create the toast element
		// ---------------------------------------------------------------------------- 
		this._toast = document.createElement('div');

		// ----------------------------------------------------------------------------
		// Set the box message.
		// If the "style" holds an icon and "no_icon" flag stays falsy, 
		// add the icon to the box message. Otherwise show the message alone
		// ---------------------------------------------------------------------------- 
		messageContent = this._icons[options.style] && !options.no_icon?
				`<span class='icon icon-${options.style}'></span><span>${msg}</span>` : msg;

		// ----------------------------------------------------------------------------
		// Generate the Toast message on the fly
		// ---------------------------------------------------------------------------- 
		var msgBox = $('<div>');
		msgBox.addClass('toast-msg');
		// ----------------------------------------------------------------------------
		// Add the message content to the box
		// ---------------------------------------------------------------------------- 
		msgBox.html(messageContent);

		// ----------------------------------------------------------------------------
		// Add the top css to the toast box plus 
		// the specific style for this box
		// ---------------------------------------------------------------------------- 
		$(this._toast).addClass('toast-box')
				.addClass('toast-' + options.style)
				.html(msgBox);			
		// -------------------------------------------------------------------
		// Append toast to the body
		// -------------------------------------------------------------------
		$('body').append(this._toast);
		// -------------------------------------------------------------------
		// Position the toast
		// in the web page
		// -------------------------------------------------------------------
		this.positionToast(options.position);		
		// -------------------------------------------------------------------
		// Show the toast
		// -------------------------------------------------------------------
		$(this._toast).fadeIn(300);

		// -------------------------------------------------------------------
		// Set a timeout for the toast duration
		// -------------------------------------------------------------------
		this._timer = setTimeout(function() { 
			// ----------------------------------------------------------------------------------------
			// Remove the toast after duration seconds
			// ----------------------------------------------------------------------------------------
			self.removeToast();
			// ----------------------------------------------------------------------------------------
			// If a callback has been specified, run it
			// ----------------------------------------------------------------------------------------
			if(options.callback)
				options.callback();
			
		}, options.duration);

		return this;
	},

	/**
	 *
	 * Position the toast
	 *
	 */
	
	positionToast : function(position){
		// -------------------------------------------------------------------
		// Calculate window width and height
		// -------------------------------------------------------------------
		var wWitdh = $(window).outerWidth();
		var wHeight = $(window).outerHeight();	
		// -------------------------------------------------------------------
		// Set the spinner box width
		// -------------------------------------------------------------------
		var toastBoxWidth = $(this._toast).width();
		// -------------------------------------------------------------------
		// Calculate the toast center horizontal position
		// -------------------------------------------------------------------
		var centerPos = (wWitdh/2) - (toastBoxWidth/2);
		// -------------------------------------------------------------------
		// Will hold the CSS properties to position the toast
		// -------------------------------------------------------------------
		var cssPosition = {};
		// -------------------------------------------------------------------
		// A generic CSS definition to set the toast in the middle of the 
		// screen
		// -------------------------------------------------------------------
		var cssPositionCenter = {
				'top' : (wHeight/2) - 100,
				'left' :  centerPos
			};

		switch(position){

			case 'center' :
				cssPosition = cssPositionCenter;
				break;

			case 'top'    : 
				cssPosition = {
					'top' : 50,
					'left' :  centerPos
				};				
				break;

			case 'bottom' : 
				cssPosition = {
					'left' : centerPos,
					'bottom' : 50
				};
				break;
			case 'top-right' : 
				cssPosition = {
					'top' : 25,
					'right' : 50
				};	
				break;
			case 'top-left'  :
				cssPosition = {
					'top' : 25,
					'left' : 50
				};
				break;
			case 'bottom-right' :
				cssPosition = {
					'bottom' : 50,
					'right' : 50
				};
				break;
			case 'bottom-left' :
				cssPosition = {
					'bottom' : 50,
					'left' : 50
				};
				break;

			default : cssPosition = cssPositionCenter;
		}
		// -------------------------------------------------------------------
		// Position the toast
		// -------------------------------------------------------------------
		$(this._toast).css(cssPosition);
	},

	removeToast : function(){
		$('.toast-box').fadeOut(500, function(e){
			this.remove();
		});		
		this._toast = null;	
	}

};
})(jQuery);